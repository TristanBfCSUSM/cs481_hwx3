﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Homework_3
{
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }
        /*Function to handle push event to Page 4*/
        void OnBackButton(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Page4());
        }

        /*Function to handle pop event to go back to the navigation stack*/
        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
