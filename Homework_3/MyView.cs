﻿using System;

using Xamarin.Forms;

namespace Homework_3
{
    public class MyView : ContentView
    {
        public MyView()
        {
            Content = new Label { Text = "Hello ContentView" };
        }
    }
}

