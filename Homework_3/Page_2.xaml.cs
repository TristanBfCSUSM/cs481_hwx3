﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Homework_3
{
    public partial class Page_2 : ContentPage
    {
        public Page_2()
        {
            InitializeComponent();
        }

        /*Function to handle push event to Page 3*/
        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Page3());
        }

        /*Function to handle pop event to go back on the navigation stack*/
        void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }

        /*Function to handle alerts showing with appearing and disappearing*/
        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("Alert of page 2", "Using OnAppear function", "Ok i give you the points");
        }

        void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("Alert of page 2", "Ok i'm also using the OnDissappear function too, so i see when you quit me :(", "Ok i give you the max points !!!");

        }
    }
}
