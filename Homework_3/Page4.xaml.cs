﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Homework_3
{
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }
        /*Function to handle pop event to go back on the navigation stack */
        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }

        /*Function to handle pop event to Root page*/
        void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            Navigation.PopToRootAsync();
        }
    }
}
